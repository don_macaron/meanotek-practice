import neuthink.metatext as metatext


# FILES SECTION
# txt
gold_dose = 'data/txt/gold_ann2a_dose.txt'
unannotated_1 = 'data/txt/unannotated_data/health4.txt'
unannotated_2 = 'data/txt/unannotated_data/health5.txt'
unannotated_3 = 'data/txt/unannotated_data/health6.txt'
smoll_boi = 'data/txt/unannotated_data/smol.txt'
drugs = 'data/txt/drugs.txt'

# csv
drugs_format = 'data/csv/drug_format_ru_ukr.csv'
drugs = 'data/csv/drugs_ru_ukr.csv'

# Model name
thingy_name = 'probably_thingy'


# HERE GOES MY BOY!
def build_model():
    print('Starting')
    data = metatext.LoadColumn(gold_dose, separator=' ')
    data = data.Unroll()
    data = data.dContextMap(source='word', target='term')
    data.enableGPU()
    data.compile()
    data.test_set.F1('term', 'dMap1', 'drug')  # ~0.6
    data.Export(thingy_name)
    print('thingy created and exported')


def use_model_or_die(text_file):
    # That's not the way, you should import things. The Great PEP Lords are not delighted
    from probably_thingy import probably_thingy as my_mighty_thingy
    data = metatext.LoadColumn(text_file, separator=' ')
    data = data.Unroll()
    res = my_mighty_thingy(data)

    return res
