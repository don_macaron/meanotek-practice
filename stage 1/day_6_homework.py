import random as rd
import numpy as np
from vpython import *



file = 'R=7.1Phi=0Theta=-10.xyz'
atoms = []
h_rad = 0.2
c_rad = 0.3

with open(file) as f:
    for line in f:
        temp = line.split()
        atoms.append((temp[0], np.array([float(temp[2]), float(temp[3]), float(temp[4])])))


def numpy_to_vp(v):
    return vec(v[0], v[1], v[2])


# print(len(atoms[0]))
def draw_molecula(dots):
    for atom in dots:
        vector = numpy_to_vp(atom[1])
        if atom[0] == 'C':
            colour = color.red
            rad = c_rad
        else:
            colour = color.blue
            rad = h_rad
        sphere(pos=vector, radius=rad, color=colour)


def distance(v1, v2):
    return np.sqrt((v2[0]-v1[0])**2 + (v2[1]-v1[1])**2 + (v2[2]-v1[2])**2)
# print(distance(atoms[0][1], atoms[7][1]))


def task_4_5(k, atms):
    res = []
    for a in atms:
        if distance(a[1], k) <= 1.5:
            res.append(a)
            v1 = numpy_to_vp(a[1])
            v2 = numpy_to_vp(k)
            cylinder(pos=v2, axis=v1-v2, radius=0.1, color=color.green)
    return res


draw_molecula(atoms)
print(task_4_5(atoms[19][1], atoms))
