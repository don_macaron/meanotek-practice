text = ["In the sample standard deviation formula, for this example, the numerator is the sum of the squared deviation of each individual animal\'s metabolic rate from the mean metabolic rate", "The table below shows the calculation of this sum of squared deviations for the female fulmars", "For females, the sum of squared deviations is 886047.09, as shown in the table"]
task_1 = lambda x: sorted(set(' '.join(text).lower().split()))

itr = lambda y: ['1' if x in text[y] else '0' for x in task_1(text)]
task_2 = [itr(x) for x in range(len(text))]
print(task_1(text))
