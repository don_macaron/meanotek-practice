import random



# не осилил сделать лямбды... :<
# 1
num = random.randrange(0,100)
print('random number =', num)
# prime = lambda num, start=2: prime(num, start + 1), False if num < 1 (True if start == num (False if num % start == 0))

def is_prime(num, start=2):
    if num < 1:
        return False
    elif start == num:
        return True
    elif(num % start==0):
        return False
    return is_prime(num ,start+1)


def prime_seq(end, start=2, res=[]):
    prime = is_prime(start)
    if start < end:
        if prime:
            return res + [start] + prime_seq(end, start+1, res)
        elif not prime:
            return res + prime_seq(end, start+1, res)
    return res

print('task 1.a: %s is %s prime' % (num, is_prime(num)))
print('task 1.b: %s' % prime_seq(num))


# 2
lst = [2,3,4,1,2,4,7,3,5]

def five_sum(lst,res=[], e=0, start=0):
    if lst[e] != lst[-1]:
        if lst[start] != lst[-1]:
            if lst[e] + lst[start] == 5:
                res.append(tuple([lst[e], lst[start]]))
                five_sum(lst,res, e, start+1)
            else:
                five_sum(lst, res, e, start+1)
        else:
            five_sum(lst, res, e+1, 0)
    elif lst[e] == lst[-1]:
        if lst[e] != lst[-1]:
            if lst[start] != lst[-1]:
                if lst[e] + lst[start] == 5:
                    res.append(tuple([lst[e], lst[start]]))
                    five_sum(lst,res, e, start+1)
                else:
                    five_sum(lst, res, e, start+1)
            else:
                return res
    return res

print('task 2:', five_sum(lst))


# 3
lst = 'words are evil, but we are blind! the true evil are digits. what do we know about them? where are they come from? none of living people couldn\'t answer this mysterious questions...'

def rsplit_tail(text, use_seps=False, result=[], seps=[',', '.', '!', '?']):
    res = result
    word = []
    if not use_seps:
        for c in text:
            if c != ' ':
                word.append(c)
            else:
                return rsplit_tail(text[len(word)+1:], False, res+[''.join(word)])
    else:
        for c in text:
            if c in seps and c != ' ':
                word.append(c)
                return rsplit_tail(text[len(word)+1:], True, res+[''.join(c)])
            elif c != ' ':
                word.append(c)
            else:
                return rsplit_tail(text[len(word)+1:], True, res+[''.join(word)])

    return res + [''.join(word)]

print('task 3:', rsplit_tail(lst, True))
