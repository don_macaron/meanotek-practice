import random as rd
import numpy as np
from vpython import *



def numpy_to_vp(v):
    return vec(v[0], v[1], v[2])

color1 = numpy_to_vp([221, 77, 67])
color2 = numpy_to_vp([252, 118, 106])
color3 = numpy_to_vp([141, 148, 64])

a = 0
b = 10
rad = 0.5
v1_np = np.array([rd.randrange(a,b),rd.randrange(a,b),rd.randrange(a,b)])
v2_np = np.array([rd.randrange(a,b),rd.randrange(a,b),rd.randrange(a,b)])
v3_np = np.array([rd.randrange(a,b),rd.randrange(a,b),rd.randrange(a,b)])


# 1
a = 0
b = 10
v1 = numpy_to_vp(v1_np)
v2 = numpy_to_vp(v2_np)
v3 = numpy_to_vp(v3_np)
print('task 1:\n', v1,v2,v3)


#2
sphere1 = sphere(pos=v1, radius=rad, color=color.red)
sphere2 = sphere(pos=v2, radius=rad, color=color.blue)
sphere3 = sphere(pos=v3, radius=rad, color=color.green)
print('task 2:\n in browser')


# 3
cylinder1 = cylinder(pos=v1, axis=v2-v1, radius=rad, color=color.red)
cylinder2 = cylinder(pos=v2, axis=v3-v2, radius=rad, color=color.blue)
cylinder3 = cylinder(pos=v3, axis=v1-v3, radius=rad, color=color.green)
print('task 3:\n in browser')


# 4
dist1 = np.linalg.norm(v1_np-v2_np) # 1 <-> 2
dist2 = np.linalg.norm(v2_np-v3_np) # 2 <-> 3
dist3 = np.linalg.norm(v3_np-v1_np) # 3 <-> 1
print('task 4:\n distance between v1 and v2: %s\n distance between v2 and v3: %s\n distance between v3 and v1: %s' % (dist1, dist2, dist3))


# 5
center_x = np.mean([v1_np[0], v2_np[0], v3_np[0]])
center_y = np.mean([v1_np[1], v2_np[1], v3_np[1]])
center_z = np.mean([v1_np[2], v2_np[2], v3_np[2]])

center_np = np.array([center_x, center_y, center_z])
center = numpy_to_vp(center_np)
center_sphere = sphere(pos=center, radius=rad)
print('task 5:\n in browser')


# 6
angle1_2 = np.dot(v1_np, v2_np)
print('task 6:\n angle beetwen V1 and V2 =', angle1_2)


# 7
# использовать нормаль плоскости, построенной из трех точек (центры сфер)
# nay, не смог разобраться в формулах >~<
# cylinder4 = cylinder(pos=center, , length=10, radius=rad, color=vec(2,3,4))
