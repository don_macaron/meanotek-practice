from functools import reduce
import math
import random
import operator


# 1
a, b = 4, 8
print('task 1:', (lambda x, y: x + y + 2)(a, b))


# 2
lst = [x*5 for x in range(200)]

lst = [x+2 if x < 100 else x for x in lst]
average = reduce(lambda x, y: x+y, lst) / len(lst)
std_deviation = math.sqrt(((reduce(lambda x, y: average - y, lst))**2)/len(lst))

# я не понимаю, где совершил ошибку. возможно не так понял условия задачи...
print('task 2:', std_deviation)
lst.clear()


# 3
lst = list(range(1,6))
sqr = reduce(lambda x, y: x*y, (lambda x: [e*e for e in x])(lst))
print('task 3:', sqr)
lst.clear()


# 4
# range использовать
mul_tab = lambda x, y: [x, y, x*y] if y in range(0, 9) and x in range(0, 9) else 'Nein'
print('task 4:', mul_tab(5, 50))


# 5
lst = ["line" for x in range(1000)]
print('task 5: \'line\'x1000')
lst.clear()


# 6
lst = [random.randrange(0, 10, 1) for x in range(10)]
res = [lst[x]+lst[x+1] if x % 2 == 0 else lst[x+1]+lst[x+2] for x in range(len(lst)-2)]
# print(lst)
print('task 6:', res)
lst.clear()


# 7
words = 'huge dog bites chicken right in the neck'


def res(x): return sorted(x.split())


def res2(x): return [x for x in sorted(x.split()) if len(x) > 3]


print('task 7: %s\ntask 7.2: %s' % (res(words), res2(words)))
