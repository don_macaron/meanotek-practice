import config
from mindy.graph import basics as bg
from mindy.graph import neo4j_graph as pg


animals = pg.PersistentGraph(config.graph_connection, graph_segment="Animals")

# ## CREATURES ##
# jackdaw = bg.Node(animals, {'type':'creature', 'name':'jackdaw'})
# walrus = bg.Node(animals, {'type':'creature', 'name':'walrus'})
# dog = bg.Node(animals, {'type':'creature', 'name':'dog'})
# pigeon = bg.Node(animals, {'type':'creature', 'name':'pigeon'})
# carp = bg.Node(animals, {'type':'creature', 'name':'carp'})
#
# ## ATTRIBUTES ##
# can_fly = bg.Node(animals, {'type':'attribute', 'name':'can fly'})
# can_swim = bg.Node(animals, {'type':'attribute', 'name':'can swim'})
# legs2 = bg.Node(animals, {'type':'attribute', 'name':'has two legs'})
# legs4 = bg.Node(animals, {'type':'attribute', 'name':'has four legs'})
#
# ## CONNECTIONS ##
# jackdaw.Connect(can_fly)
# jackdaw.Connect(legs2)
# walrus.Connect(can_swim)
# dog.Connect(legs4)
# pigeon.Connect(legs2)
# pigeon.Connect(can_fly)
# carp.Connect(can_swim)


attr_list = [x['name'] for x in animals.Match({'type':'attribute'})]
creature_list = [x['name'] for x in animals.Match({'type':'creature'})]

run = True
while(run):
    used_creatures = []
    creatures = animals.Match({'type':'creature'})
    attr_count = 0
    current_animal = animals.MatchOne({'type':'creature'})
    creature_attributes = animals.MatchOne({'type':'attribute'})
    for c in creatures:
        if len(c.Children({})) > attr_count:
            attr_count = len(c.Children({}))
            if not c in used_creatures:
                current_animal = c
                creature_attributes = c.Children({})
            else:
                used_creatures.append(c)

    answer_attr = []
    for ca in creature_attributes:
        print('%s?' % ca['name'])
        answer = input()
        if answer.lower() == 'yes':
            answer_attr.append(ca['name'])
