from tco import *
import random



# 1
lst = [1,2,3,4,5,6,7,8,9,10]
lst_reverse = lambda x: [x[-1]] + lst_reverse(x[:-1]) if len(x) > 0 else []
print('task 1:', lst_reverse(lst))
lst.clear()


# 2
# print('task 2: ちょっとむじかしいですよ。。。')


# 3
lst = [[2,3],[3,4,[3,4,[4,5]]]]
flatomatic = lambda x: sum(map(flatomatic, x), []) if isinstance(x, list) else [x]
print('task 3:', flatomatic(lst))
lst.clear()


# 4
lst = 'words are evil, but we are blind! the true evil are digits. what do we know about them? where are they come from? none of living people couldn\'t answer this mysterious questions...'
def rsplit(text):
    word = []
    res = []
    for c in text:
        if c != ' ':
            word.append(c)
        else:
            return res + [''.join(word)] + rsplit(text[len(word)+1:])
    return res + [''.join(word)]

print('task 4:', rsplit(lst))
# print('task 4: ちょっとむじかしいですよ。。。')


# 5
lst = [4, 3, 1, 8, 6, 7]
shifter = lambda x: [tuple(x[0:2])]+shifter(x[2:]) if len(x) >= 2 else []
print('task 5:', shifter(lst))


# 6
long_lst = [1,3,4,8,6,4,3,4,5]
lst = [3, 2, 7]
tri_force = lambda l, s: [a*b for a,b in zip(l[:3], s)] + tri_force(l[3:], s) if len(l) >= 3 else []
print('task 6:',tri_force(long_lst, lst))
