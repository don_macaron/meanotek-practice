def rsplit(text, use_seps=False, get_pos=True, _result=[], _seps=[',', '.', '!', '?']) -> list:
    """ Splits string by whitespaces

    Parameters:
        text (str):Text which is to be separated into words
        use_seps (bool):Flag which is telling the function to include/exclude separators, e.g. punctuation
        get_pos(bool):Get starts and ends of words or not

    Returns:
        words(list):List of words or list of words with its coordinates
    """
    res: list = _result
    word: list = []
    seps: bool = use_seps
    pos: bool = get_pos
    # Checks if "separators" are used.
    # If they are, then words in returned list will be without punctation
    if not use_seps:
        # c - stands for character
        for c in text:
            if c != ' ':
                word.append(c)
            else:
                return rsplit(text[len(word)+1:], seps, pos, res+[''.join(word)])
    else:
        for c in text:
            if c in _seps and c != ' ':
                word.append(c)
                return rsplit(text[len(word)+1:], seps, pos, res+[''.join(c)])
            elif c != ' ':
                word.append(c)
            else:
                return rsplit(text[len(word)+1:], seps, pos, res+[''.join(word)])
    if pos:
        return list(zip(res + [''.join(word)], word_pos(' '.join(res + [''.join(word)]))))
    else:
        return res + [''.join(word)]


def word_pos(text:str) -> list:
    """ Gets start and end indices of every word

    Parameters:
        text(str):Input string
    Returns:
        res(list):List of tuples with start and end

    """
    res: list = []
    start: int = 0
    end: int = 0
    for i in range(len(text)):
        if text[i] != ' ':
            end += 1
        elif text[i] == ' ':
            res.append([start, end])
            end = i
            start = i + 1
    return res + [[start, end]]

print(rsplit('i do like dogs, they are adorable and tasty', True, True))
