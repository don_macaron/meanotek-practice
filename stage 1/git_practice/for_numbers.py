def odds_evens(nums):
    odds = []
    evens = []
    for n in nums:
        if n % 2 == 0:
            odds.append(n)
        else:
            evens.append(n)
    return (odds, evens)
