def odds_evens(nums, res=[[],[]]):
    if len(nums) > 0:
        if nums[0] % 2 == 0:
            res[0].append(nums[0])
            return odds_evens(nums[1:], res)
        else:
            res[1].append(nums[0])
            return odds_evens(nums[1:], res)
    return res
