def odds_evens(nums):
    odds = [x for x in nums if x % 2 == 0]
    evens = [x for x in nums if x % 2 != 0]
    return (odds, evens)
