def odds_evens(nums):
    odds = list(filter(lambda x: x%2 == 0, nums))
    evens = list(filter(lambda x: x%2 != 0, nums))
    return (odds, evens)
