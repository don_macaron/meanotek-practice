class animal:
    def __init__(self, name):
        self.name = name
        self.attributes = {'size':'no_data', 'can fly':'no_data',
                    'can swim':'no_data', 'eats':'no_data'}
    def __str__(self):
        return self.name

    def change_attribute(self, attr, val):
        self.attributes[attr] = val




def main_loop():
    pigeon = animal('pigeon')
    pigeon.change_attribute('size', 'small')
    pigeon.change_attribute('can fly', 'yes')
    pigeon.change_attribute('can swim', 'no')
    pigeon.change_attribute('eats', 'seeds')
    pigeon.change_attribute('lives', 'cities')

    lion = animal('lion')
    lion.change_attribute('size', 'normal')
    lion.change_attribute('can fly', 'no')
    lion.change_attribute('can swim', 'no')
    lion.change_attribute('eats', 'meat')

    walrus = animal('walrus')
    walrus.change_attribute('size', 'big')
    walrus.change_attribute('can fly', 'no')
    walrus.change_attribute('can swim', 'yes')
    walrus.change_attribute('eats', 'meat')

    animals = [pigeon, lion, walrus]

    print('hello, meatbag. Your animal...')
    temp_animal = animal('bob')

    answer = input('is it big? ').lower()
    if answer == 'yes':
        temp_animal.change_attribute('size', 'big')
    elif answer == 'no':
        answer = input('is it small? ').lower()
        if answer == 'yes':
            temp_animal.change_attribute('size', 'small')
        elif answer == 'no':
            temp_animal.change_attribute('size', 'normal')

    answer = input('can it fly? ').lower()
    if answer == 'yes':
        temp_animal.change_attribute('can fly', 'yes')
    elif answer == 'no':
        temp_animal.change_attribute('can fly', 'no')

    answer = input('can it swim? ').lower()
    if answer == 'yes':
        temp_animal.change_attribute('can swim', 'yes')
    elif answer == 'no':
        temp_animal.change_attribute('can swim', 'no')

    answer = input('... does it eat meat? ').lower()
    if answer == 'yes':
        temp_animal.change_attribute('eats', 'meat')
    elif answer == 'no':
        answer = input('does it eat grass? ').lower()
        if answer == 'yes':
            temp_animal.change_attribute('eats', 'grass')
        elif answer == 'no':
            temp_animal.change_attribute('eats', 'seeds')


    # check current state of temp_animal with all
    # existing animals e.g. pigeon, lion
    # print(temp_animal.attributes)
    your_animal = check(temp_animal.attributes, animals)[0]
    print('is it %s?' % your_animal)
    answer = input().lower()



def check(answer, animals):
    matches = 0
    conclusion = {}
    for animal in animals:
        for ans_val, animal_val in zip(answer.values(), animal.attributes.values()):
            if ans_val == animal_val:
                matches += 1
        conclusion[animal.name] = matches
        matches = 0
        print(type(answer))
    return max(conclusion.items(), key=lambda k: k[1])

main_loop()
